package com.epam.test.automation.java.practice5;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ArrayRectanglesTest {

    Rectangle rec1 = new Rectangle(5, 10);
    Rectangle rec2 = new Rectangle(5, 5);
    Rectangle rec3 = new Rectangle(20, 1);
    Rectangle rec4 = new Rectangle(15, 15);
    Rectangle rec5 = new Rectangle(15, 15);

    ArrayRectangles arr = new ArrayRectangles(rec1, rec2, rec3, rec4);
   // ArrayRectangles arr2 = new ArrayRectangles(3);


    @Test
    public void testAddRectangle() {
        Assert.assertEquals(arr.addRectangle(rec5),false);
    }

    @Test
    public void testNumberMaxArea() {
        Assert.assertEquals(arr.numberMaxArea(),3);
    }

    @Test
    public void testNumberMinPerimeter() {
        Assert.assertEquals(arr.numberMinPerimeter(),1);
    }
    @Test
    public void testNumberSquares() {
        Assert.assertEquals(arr.numberSquares(),2);
    }
}