package com.epam.test.automation.java.practice5;


public class ArrayRectangles {
    private Rectangle[] rectangleArray;

    public ArrayRectangles(int n) {
        this.rectangleArray = new Rectangle[n];
    }

    public ArrayRectangles(Rectangle ... rectangleArray) {
        this.rectangleArray = new Rectangle[rectangleArray.length];
        for (int i = 0; i < rectangleArray.length; i++) {
            this.rectangleArray[i] = rectangleArray[i];
        }
    }

    public boolean addRectangle(Rectangle rectangle) {
        for (int i = 0; i < rectangleArray.length - 1; i++) {
            if (rectangleArray[i] == null) {
                rectangleArray[i] = rectangle;
                return true;
            }
        }
        return false;

    }


    public int numberMaxArea() {

        double maxArea = 0;
        int numberMaxArea = -1;

            for (int i = 0; i < rectangleArray.length; i++) {


                if (rectangleArray[i] != null && maxArea < rectangleArray[i].area()) {
                    numberMaxArea = i;

                    maxArea = rectangleArray[i].area();
                }

            }

            return numberMaxArea;
        }




    public int numberMinPerimeter() {
        int numberMinPerimeter = -1;
        if (rectangleArray[0] != null) {

            double minPerimeter = rectangleArray[0].perimeter();

            for (int i = 1; i < rectangleArray.length; i++) {

                if (rectangleArray[i] != null && minPerimeter > rectangleArray[i].perimeter()) {
                    numberMinPerimeter = i;
                    minPerimeter = rectangleArray[i].perimeter();
                }

            }

            return numberMinPerimeter;
        }
        return -1;

    }


    public int numberSquares() {
        int numberSquare = 0;
        for (int i = 0; i < rectangleArray.length; i++) {
            if (rectangleArray[i] != null && rectangleArray[i].isSquare()) {
                numberSquare++;

            }
        }
        return numberSquare;
    }

}


