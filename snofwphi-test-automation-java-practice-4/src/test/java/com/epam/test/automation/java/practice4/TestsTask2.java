package com.epam.test.automation.java.practice4;

import org.testng.annotations.Test;


import static org.testng.internal.junit.ArrayAsserts.assertArrayEquals;

public class TestsTask2 {
    @Test
    public void Test2()
    {
        int [] array= {5, 17, 24, 88, 33, 2};
        Task2.transform(array,SortOrder.ASC);
        int[] expectedArray = {5, 17, 24, 88, 33, 2};
        assertArrayEquals(expectedArray,array);
    }
    @Test
    public void Test22()
    {
        int [] array= {15,10,3};
        Task2.transform(array,SortOrder.DESC);
        int[] expectedArray = {15, 11, 5};
        assertArrayEquals(expectedArray,array);
    }

}
