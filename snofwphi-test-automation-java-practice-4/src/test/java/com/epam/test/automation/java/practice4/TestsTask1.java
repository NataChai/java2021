package com.epam.test.automation.java.practice4;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.internal.junit.ArrayAsserts.assertArrayEquals;

public class TestsTask1 {
    @Test
    public void Test1()
    {
        int [] array= {5, 17, 24, 88, 33, 2};

        assertEquals(false,Task1.isSorted(array,SortOrder.ASC));
    }
    @Test
    public void Test11()
    {
        int [] array= {5, 17, 24, 88, 100, 102};

        assertEquals(true,Task1.isSorted(array,SortOrder.ASC));
    }
}
