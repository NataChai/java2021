package com.epam.test.automation.java.practice4;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

public class TestsTask4 {
    @Test
    public void Test4() {

        assertEquals(175.0, Task4.sumGeometricElements(100, 0.5, 20));
    }
    @Test(expectedExceptions = IllegalArgumentException.class)
    public void Test41() {


        Task4.sumGeometricElements(100, 0, 20);

    }
    @Test(expectedExceptions = IllegalArgumentException.class)
    public void Test42() {


        Task4.sumGeometricElements(100, 0.5, 200);

    }

}
