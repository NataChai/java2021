package com.epam.test.automation.java.practice4;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

public final class TestsTask3 {
    @Test
    public void Test3() {


        assertEquals(6160, Task3.multiArithmeticElements(5, 3, 4));

    }
    @Test(expectedExceptions = IllegalArgumentException.class)
    public void Test31() {


        Task3.multiArithmeticElements(1, 2, 0);

    }
    @Test(expectedExceptions = IllegalArgumentException.class)
    public void Test32() {


        Task3.multiArithmeticElements(1, 2, -3);

    }
}
