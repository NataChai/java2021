package com.epam.test.automation.java.practice4;

public class Task3 {
    private Task3() {
    }

    public static int multiArithmeticElements(int a1, int t, int n) {
        if (a1 > 2147483647 || a1 < -2147483648 || n <= 0)
            throw new IllegalArgumentException();
        else {
            int res = 1;

            int i = 0;
            while (i < n) {
                res = res * (a1 + i * t);
                i++;
            }
            return res;

        }
    }
}
