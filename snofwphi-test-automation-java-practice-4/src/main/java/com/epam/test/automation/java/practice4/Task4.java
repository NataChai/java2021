package com.epam.test.automation.java.practice4;

public class Task4 {

    private Task4() {
    }

    public static double sumGeometricElements(int a1, double t, int alim) {
        if (a1 > 2147483647 || a1 < -2147483648 || a1 <= alim || alim > 2147483647 || alim < -2147483648 || t <= 0 || t > 1)
            throw new IllegalArgumentException();

        else {
            double sum = 0;

            sum = a1;
            while (a1 * t > alim) {
                sum += a1 * t;
                a1 *= t;
            }

            return sum;
        }
    }
}
