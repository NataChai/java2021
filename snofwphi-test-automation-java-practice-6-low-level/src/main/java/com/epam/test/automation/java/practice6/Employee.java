package com.epam.test.automation.java.practice6;
import java.math.BigDecimal;

public abstract class Employee {
    private String lastName;
    private BigDecimal salary;
    protected BigDecimal bonus;

    protected Employee(String lastName, BigDecimal salary) {
        this.lastName = lastName;
        this.salary = salary;
    }


    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public BigDecimal getBonus() {
        return bonus;
    }

    public abstract void setBonus(BigDecimal bonus);


    public BigDecimal toPay() {
        return salary.add(bonus);
    }


}

