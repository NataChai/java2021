package com.epam.test.automation.java.practice6;


import java.math.BigDecimal;

public class SalesPerson extends Employee {
    private int percent;

    public SalesPerson(String name, BigDecimal salary, int percent) {

        super(name, salary);
        this.percent = percent;

    }

    @Override
    public void setBonus(BigDecimal bonus) {
        if (bonus != null && (bonus.compareTo(BigDecimal.ZERO) > 0)) {
            if (percent <= 200 && percent > 100)
                this.bonus = bonus.multiply(BigDecimal.valueOf(2));
            else if (percent > 200) {
                this.bonus = bonus.multiply(BigDecimal.valueOf(3));
            } else
                this.bonus = bonus;

        } else throw new IllegalArgumentException();

    }
}
