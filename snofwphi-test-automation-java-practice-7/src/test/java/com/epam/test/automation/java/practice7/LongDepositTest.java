package com.epam.test.automation.java.practice7;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class LongDepositTest {
    Deposit ld = new LongDeposit(new BigDecimal(3000),7);
    Deposit ld2 = new LongDeposit(new BigDecimal(1000),2);
    @Test
    public void testTestIncome3000and7() {
        Assert.assertEquals(ld.income(),new BigDecimal(450).setScale(2, RoundingMode.HALF_UP ));
    }
    @Test
    public void testTestIncome1000and2() {
        Assert.assertEquals(ld2.income(),0);
    }
}