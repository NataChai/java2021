package com.epam.test.automation.java.practice7;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class BaseDepositTest {
    Deposit bd = new BaseDeposit(new BigDecimal(3000),6);
    Deposit bd2 = new BaseDeposit(new BigDecimal(1000),3);
    @Test
    public void testTestIncome3000and6() {
        Assert.assertEquals(bd.income(),new BigDecimal(1020.29).setScale(2, RoundingMode.HALF_UP ));
    }
    @Test
    public void testTestIncome1000and3() {
        Assert.assertEquals(bd2.income(),new BigDecimal(157.62).setScale(2, RoundingMode.HALF_UP ));
    }
}