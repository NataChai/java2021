package com.epam.test.automation.java.practice7;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class SpecialDepositTest {
    Deposit sd = new SpecialDeposit(new BigDecimal(3000),6);

    @Test
    public void testTestIncome() {
        Assert.assertEquals(sd.income(),new BigDecimal(684.75).setScale(2, RoundingMode.HALF_UP ));
    }
}