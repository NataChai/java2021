package com.epam.test.automation.java.practice7;

import java.math.BigDecimal;
import java.math.RoundingMode;


public class BaseDeposit extends Deposit{

    public BaseDeposit(BigDecimal depositAmount, int depositPeriod) {
        super(depositAmount, depositPeriod);
    }

    @Override
    public BigDecimal income() {
        int i = 1;
        BigDecimal incomeTotal = super.getAmount();
        while (i <= super.getPeriod())
        {
            incomeTotal = incomeTotal.multiply((BigDecimal.valueOf(0.05)) ).add(incomeTotal);
            i++;
        }
        return incomeTotal.subtract(super.getAmount()).setScale(2, RoundingMode.HALF_EVEN);

    }


}
