package com.epam.test.automation.java.practice7;


import java.math.BigDecimal;
import java.math.RoundingMode;



public class LongDeposit extends Deposit{


    protected LongDeposit(BigDecimal depositAmount, int depositPeriod) {
        super(depositAmount, depositPeriod);
    }

    @Override
    public BigDecimal income() {
        BigDecimal incomeTotal = super.getAmount();
        if (super.getPeriod() <= 6)
            return BigDecimal.ZERO.setScale(0);
        else
        {
            int i = 6;
            while (i < super.getPeriod())
            {
                incomeTotal = incomeTotal.multiply(BigDecimal.valueOf(0.15)).add(incomeTotal).setScale(2, RoundingMode.HALF_EVEN);
                i++;
            }
        }
        return incomeTotal.subtract(super.getAmount()).setScale(2, RoundingMode.HALF_EVEN);

    }
}
