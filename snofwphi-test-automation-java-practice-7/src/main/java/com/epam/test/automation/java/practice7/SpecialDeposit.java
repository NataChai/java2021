package com.epam.test.automation.java.practice7;

import java.math.BigDecimal;
import java.math.RoundingMode;


public class SpecialDeposit extends Deposit{

    protected SpecialDeposit(BigDecimal depositAmount, int depositPeriod) {
        super(depositAmount, depositPeriod);
    }

    @Override
    public BigDecimal income() {
        int i = 1;
        BigDecimal incomeTotal = super.getAmount();
        while (i <= super.getPeriod())
        {
            incomeTotal = (incomeTotal.multiply(BigDecimal.valueOf(i))).divide(BigDecimal.valueOf(100)).add(incomeTotal).setScale(2,RoundingMode.HALF_EVEN);
            i++;
        }
        return incomeTotal.subtract(super.getAmount()).setScale(2,RoundingMode.HALF_EVEN);

    }
}
