package com.epam.test.automation.java.practice7;


import java.math.BigDecimal;
import java.math.RoundingMode;

public class Client {
    private Deposit [] deposits;
    public Client()
    {
        deposits = new Deposit[10];
    }
    public boolean addDeposit(Deposit deposit)
    {
        for (int i = 0; i < 10; i++)
        {
            if (deposits[i] == null)
            {
                deposits[i] = deposit;
                return true;
            }
        }
        return false;
    }
    public BigDecimal totalIncome()
    {
        BigDecimal income = BigDecimal.ZERO;
        for (Deposit deposit : deposits)
        {
            if (deposit != null)
                income =income.add(deposit.income()).setScale(2,RoundingMode.HALF_EVEN);
        }
        return income.setScale(2,RoundingMode.HALF_EVEN);
    }
    public BigDecimal maxIncome()
    {
        BigDecimal income = BigDecimal.ZERO;
        for (Deposit deposit : deposits)
        {
            if (deposit != null && income.compareTo(deposit.income())<0)
                income = deposit.income().setScale(2,RoundingMode.HALF_EVEN);
        }
        return income.setScale(2,RoundingMode.HALF_EVEN);
    }
    public BigDecimal getIncomeByNumber(int i)
    {
        if (deposits[i - 1] != null)
            return deposits[i ].income().setScale(2, RoundingMode.HALF_EVEN);
        else
            return BigDecimal.ZERO;
    }



}
