package com.epam.test.automation.java.practice3;


import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.internal.junit.ArrayAsserts.assertArrayEquals;

public class MainTest {

    @Test
    public void Test1() {
        int[] expectedArray = {4, 5, 3, 10};
        int[] argumentArray = {10, 5, 3, 4};
        Main.task1(argumentArray);

        assertArrayEquals(expectedArray, argumentArray);
    }
    @Test
    public void Test11() {
        int[] expectedArray = {54, 4, 3, 45, 33, 8, 2, 100};
        int[] argumentArray = {100, 2, 3, 45, 33, 8, 4, 54};
        Main.task1(argumentArray);

        assertArrayEquals(expectedArray, argumentArray);
    }
    @Test
    public void Test12() {
        int[] expectedArray = {3, 5, 7, 9, 11, 13, 15, 17};
        int[] argumentArray = {3, 5, 7, 9, 11, 13, 15, 17};
        Main.task1(argumentArray);

        assertArrayEquals(expectedArray, argumentArray);
    }
    @Test(expectedExceptions = IllegalArgumentException.class)
    public void Test13() {

        int[] argumentArray = {};
        Main.task1(argumentArray);


    }

    @Test
    public void Test2() {
        int[] argumentArray = {5, 350, 350, 4, 350};


        assertEquals(3, Main.task2(argumentArray));
    }

    @Test
    public void Test21() {
        int[] argumentArray = {100};

        assertEquals(0, Main.task2(argumentArray));
    }

    @Test
    public void Test22() {
        int[] argumentArray = {100, 100};

        assertEquals(1, Main.task2(argumentArray));
    }

    @Test
    public void Test23() {
        int[] argumentArray = {5, 350, 350, 4, 350};

        assertEquals(3, Main.task2(argumentArray));
    }

    @Test
    public void Test24() {
        int[] argumentArray = {10, 10, 10, 10, 10};

        assertEquals(4, Main.task2(argumentArray));
    }

    @Test
    public void Test3() {
        int[][] expectedArray = {{2, 1, 1, 1}, {0, 7, 1, 1}, {0, 0, 3, 1}, {0, 0, 0, 5}};
        int[][] argumentArray = {{2, 4, 3, 3}, {5, 7, 8, 5}, {2, 4, 3, 3}, {5, 7, 8, 5}};
        Main.task3(argumentArray);

        assertArrayEquals(expectedArray, argumentArray);
    }

}