package com.epam.test.automation.java.practice3;


public class Main {

    private Main() {
    }

    public static int[] task1(int[] array) {
        if (array.length <= 0) throw new IllegalArgumentException();
        else {
            int tmp;

            for (int i = 0; i < (array.length / 2); i++) {
                if (array[i] % 2 == 0 && array[array.length - 1 - i] % 2 == 0) {
                    tmp = array[i];
                    array[i] = array[array.length - 1 - i];
                    array[array.length - 1 - i] = tmp;
                }
            }
            return array;
        }

    }

    public static int maxArray(int[] array) {
        if (array.length <= 0) throw new IllegalArgumentException();
        else {
            int max = 0;

            for (int i = 0; i < array.length; i++) {
                if (array[i] > max) {
                    max = array[i];
                }
            }
            return max;
        }
    }

    public static int getArrayIndex(int[] arr, int value) {
        for (int i = 0; i < arr.length; i++)
            if (arr[i] == value) return i;
        return -1;
    }

    public static int getArrayLastIndex(int[] arr, int value) {
        for (int i = arr.length - 1; i >= 0; i--)
            if (arr[i] == value) return i;
        return -1;
    }

    public static int task2(int[] array) {
        if (array.length <= 0) throw new IllegalArgumentException();
        else {
            int distance = 0;
            int maxValue = maxArray(array);

            distance = getArrayLastIndex(array, maxValue) - getArrayIndex(array, maxValue);
            return distance;

        }
    }

    public static int[][] task3(int[][] matrix) {
        if (matrix.length <= 0) throw new IllegalArgumentException();
        else {

            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[i].length; j++) {
                    if (i < j) {
                        matrix[i][j] = 1;
                    } else if (i > j) {
                        matrix[i][j] = 0;
                    }
                }
            }
            return matrix;
        }
    }
}




