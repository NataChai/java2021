package com.epam.test.automation.java.practice2;


public class Main {
    public void main() {
        throw new UnsupportedOperationException();
    }

    public static int task1(int value) {

            if(value <= 0) throw new IllegalArgumentException();

        int result = 0;
        while (value > 0)
        {
            int sign = value % 10;
            if (sign % 2 == 1)
                result += sign;
            value /= 10;
        }
        return result;

    }

        public static int task2(int value) {
            if(value <= 0) throw new IllegalArgumentException();

            int result = 0;
            while (value != 0)
            {
                if (value % 2 == 1)
                    result++;
                value /= 2;
            }
            return result;

        }


    public static int task3(int value) {
        if(value <= 0) throw new IllegalArgumentException();

        int sum = 0;
        int tmp = 1;
        int result = 0;
        while (value  > 1)
        {
            sum = tmp + sum;
            tmp = sum - tmp;
            result += sum;
            value--;
        }

        return result;

    }

}

