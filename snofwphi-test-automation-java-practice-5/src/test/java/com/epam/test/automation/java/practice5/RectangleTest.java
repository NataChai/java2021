package com.epam.test.automation.java.practice5;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

public class RectangleTest {
    Rectangle rec = new Rectangle(5, 10);
    Rectangle square = new Rectangle(5, 5);
    @Test
    public void testArea() {
        double expectedResult = 50;
        assertEquals(expectedResult, rec.area());
    }
    @Test
    public void testPerimeter() {
        double expectedResult = 30;
        assertEquals(expectedResult, rec.perimeter());
    }
    @Test
    public void testIsSquare() {

        assertEquals(true, square.isSquare());

    }
    @Test
    public void testIsSquareFalse() {

        assertEquals(false, rec.isSquare());

    }
    @Test
    public void testReplaceSides() {
rec.replaceSides();
        assertEquals(10.0,rec.getSideA());
        assertEquals(5.0,rec.getSideB());
    }
}