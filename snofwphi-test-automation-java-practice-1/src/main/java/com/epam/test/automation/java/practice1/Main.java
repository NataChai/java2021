package com.epam.test.automation.java.practice1;


public class Main {
    void main() {
throw new UnsupportedOperationException();
//sonar
    }

    public static int task1(int n) {
        int result;
        if (n>0)
        {
            result = n*n;
        }
        else result = Math.abs(n);
        return result;
    }

    public static int task2(int n) 
    {
         int tmp;
            
            int first = n / 100;
            int second = (n % 100) / 10;
            int third = n % 10;

            if (first < second)
            {
                tmp = first;
                first = second;
                second = tmp;
            }
            
            if (first < third)
            {
                tmp = first;
                first = third;
                third = tmp;
            }
            if (second <third)
            {
                tmp=second;
                second = third;
                third = tmp;
            }

            return first * 100 + second * 10 + third;
    }


}
