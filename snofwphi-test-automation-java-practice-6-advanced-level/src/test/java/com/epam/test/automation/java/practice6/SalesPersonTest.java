package com.epam.test.automation.java.practice6;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.math.BigDecimal;

public class SalesPersonTest {
    SalesPerson s1 = new SalesPerson("Ben", new BigDecimal(150), 101);
    SalesPerson s2 = new SalesPerson("Ben", new BigDecimal(150), 250);


    @Test
    public void testSetBonus101() {
        s1.setBonus(new BigDecimal(101));
        Assert.assertEquals(s1.getBonus(),new BigDecimal(202));

    }
    @Test
    public void testSetBonus0() {
        s2.setBonus(new BigDecimal(205));
        Assert.assertEquals(s2.getBonus(),new BigDecimal(615));

    }
}
