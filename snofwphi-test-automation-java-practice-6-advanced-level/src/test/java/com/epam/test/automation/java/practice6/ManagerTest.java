package com.epam.test.automation.java.practice6;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.math.BigDecimal;


public class ManagerTest {
    Manager m1 = new Manager("Ben", new BigDecimal(150), 101);

    @Test
    public void testSetBonus101() {
        m1.setBonus(new BigDecimal(100));
        Assert.assertEquals(m1.getBonus(),new BigDecimal(600));

    }
    @Test
    public void testSetBonus0() {
        m1.setBonus(new BigDecimal(101));
        Assert.assertEquals(m1.getBonus(),new BigDecimal(601));

    }
}
