package com.epam.test.automation.java.practice6;


import org.testng.Assert;
import org.testng.annotations.Test;

import java.math.BigDecimal;

public class CompanyTest {
    SalesPerson s1 = new SalesPerson("Ben", new BigDecimal(150), 101);
    SalesPerson s2 = new SalesPerson("Ben", new BigDecimal(150), 250);
    Manager m1 = new Manager("Tom", new BigDecimal(1500), 101);

    Company staff = new Company(s1, s2, m1);


    @Test
    public void TestNameMaxSalary()
    {
        staff.giveEverybodyBonus(new BigDecimal(50));
        Assert.assertEquals(staff.nameMaxSalary(),"Tom");

    }

    @Test
    public void testGiveEverybodyBonus() {
        staff.giveEverybodyBonus(new BigDecimal(50));
        Assert.assertEquals(s1.getBonus(),new BigDecimal(100));

    }
    @Test
    public void testTotalToPay() {
        staff.giveEverybodyBonus(new BigDecimal(50));
        staff.totalToPay();
        Assert.assertEquals(s1.toPay(),new BigDecimal(250));
    }

}
