package com.epam.test.automation.java.practice6;

import java.math.BigDecimal;


public class Company {
    private Employee[] employees;

    public Company(Employee... staff) {
        this.employees = new Employee[staff.length];
        for (int i = 0; i < staff.length; i++) {
            this.employees[i] = staff[i];
        }

    }

    public void giveEverybodyBonus(BigDecimal companyBonus) {
        for (Employee i : employees) {
            i.setBonus(companyBonus);

        }
    }

    public BigDecimal totalToPay() {
        BigDecimal totalToPay = new BigDecimal(0);

        for (Employee i : employees) {
            totalToPay = totalToPay.add(i.toPay());

        }
        return totalToPay;

    }

    public String nameMaxSalary() {
        String nameMaxSalary = "";
        if (employees.length != 0) {
            BigDecimal maxSalary = new BigDecimal(0);
            for (Employee i : employees) {

                if (i.getSalary().compareTo(maxSalary)>0) {
                    maxSalary = i.getSalary();
                    nameMaxSalary = i.getLastName();
                }

            }
        }
        return nameMaxSalary;

    }
}



