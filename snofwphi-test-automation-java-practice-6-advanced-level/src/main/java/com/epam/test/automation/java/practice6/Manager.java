package com.epam.test.automation.java.practice6;

import java.math.BigDecimal;

public class Manager extends Employee{
    private int quantity;

    public Manager(String lastName, BigDecimal salary, int clientAmount) {
        super(lastName, salary);
        this.quantity = clientAmount;

    }


    @Override
    public void setBonus(BigDecimal bonus) {

        if (bonus != null && (bonus.compareTo(BigDecimal.ZERO) > 0)) {
            if (quantity > 150)
                this.bonus = bonus.add(new BigDecimal(1000));

            else if (quantity > 100 && quantity <= 150)
                this.bonus = bonus.add(new BigDecimal(500));

            else this.bonus=bonus;

        } else
            throw new IllegalArgumentException();

    }
}