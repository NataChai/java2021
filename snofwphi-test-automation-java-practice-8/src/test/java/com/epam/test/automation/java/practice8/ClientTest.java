package com.epam.test.automation.java.practice8;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class ClientTest {
    Deposit bd = new BaseDeposit(new BigDecimal(3000),6);
    Deposit bd2 = new BaseDeposit(new BigDecimal(1000),3);
    Deposit ld = new LongDeposit(new BigDecimal(3000),7);
    Deposit ld2 = new LongDeposit(new BigDecimal(3000),36);
    Client deposits=new Client();



    @Test
    public void testTestAddDeposit() {

        deposits.addDeposit(bd);
        deposits.addDeposit(ld);
        Assert.assertTrue(deposits.addDeposit(bd2));
    }

    @Test
    public void testTestTotalIncome() {
        deposits.addDeposit(bd);
        deposits.addDeposit(ld);

        Assert.assertEquals(deposits.totalIncome(), new BigDecimal(1470.29).setScale(2, RoundingMode.HALF_UP));

    }

    @Test
    public void testTestMaxIncome() {
        deposits.addDeposit(bd);
        Assert.assertEquals(deposits.getIncomeByNumber(1), new BigDecimal(1020.29).setScale(2, RoundingMode.HALF_UP));
    }
    @Test
    public void testSortDeposits() {
        deposits.addDeposit(bd);
        deposits.addDeposit(ld2);
        deposits.addDeposit(ld);
        deposits.sortDeposits();

    }
    @Test
    public void testCountPossibleToProlongDeposit() {
        deposits.addDeposit(bd);
        deposits.addDeposit(ld2);
        deposits.addDeposit(ld);
        Assert.assertEquals(deposits.countPossibleToProlongDeposit(),1);
    }
}
