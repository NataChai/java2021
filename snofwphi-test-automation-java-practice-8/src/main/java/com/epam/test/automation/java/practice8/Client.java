package com.epam.test.automation.java.practice8;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class Client implements Iterable<Deposit> {
    private Deposit[] deposits;

    public Client() {
        deposits = new Deposit[10];
    }

    public boolean addDeposit(Deposit deposit) {
        for (int i = 0; i < 10; i++) {
            if (deposits[i] == null) {
                deposits[i] = deposit;
                return true;
            }
        }
        return false;
    }

    public BigDecimal totalIncome() {
        BigDecimal income = BigDecimal.ZERO;
        for (Deposit deposit : deposits) {
            if (deposit != null)
                income = income.add(deposit.income()).setScale(2, RoundingMode.HALF_EVEN);
        }
        return income.setScale(2, RoundingMode.HALF_EVEN);
    }

    public BigDecimal maxIncome() {
        BigDecimal income = BigDecimal.ZERO;
        for (Deposit deposit : deposits) {
            if (deposit != null && income.compareTo(deposit.income()) < 0)
                income = deposit.income().setScale(2, RoundingMode.HALF_EVEN);
        }
        return income.setScale(2, RoundingMode.HALF_EVEN);
    }

    public BigDecimal getIncomeByNumber(int i) {
        if (deposits[i - 1] != null)
            return deposits[i].income().setScale(2, RoundingMode.HALF_EVEN);
        else
            return BigDecimal.ZERO;
    }


    @Override
    public Iterator<Deposit> iterator()  {

        return new Iterator() {
            @Override
            public boolean hasNext() {
                return false;
            }

            @Override
            public Object next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }

                return true;

            }
        };
    }

    public void sortDeposits() {
        Arrays.sort(deposits);
        List<Object> list = Arrays.asList(deposits);
        Collections.reverse(list);
    }

    public int countPossibleToProlongDeposit() {
        int amountPossibleToProlongDeposit = 0;


        for (Deposit deposit : deposits) {
            if (deposit instanceof Prolongable && ((Prolongable) deposit).canToProlong()) {
                amountPossibleToProlongDeposit++;
            }
        }

        return amountPossibleToProlongDeposit;
    }
}
