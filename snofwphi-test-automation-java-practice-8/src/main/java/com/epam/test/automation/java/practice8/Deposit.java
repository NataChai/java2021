package com.epam.test.automation.java.practice8;

import java.math.BigDecimal;


public abstract class Deposit implements Comparable<Deposit> {
    public BigDecimal getAmount() {
        return amount;
    }

    public final  BigDecimal amount;

    public int getPeriod() {
        return period;
    }

    public final int period;

    protected Deposit(BigDecimal depositAmount, int depositPeriod) {
        this.amount = depositAmount;
        this.period = depositPeriod;
    }


    public abstract BigDecimal income();


}