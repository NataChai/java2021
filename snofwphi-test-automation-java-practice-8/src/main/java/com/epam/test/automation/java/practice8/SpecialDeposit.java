package com.epam.test.automation.java.practice8;

import java.math.BigDecimal;
import java.math.RoundingMode;


public class SpecialDeposit extends Deposit implements Prolongable{

    protected SpecialDeposit(BigDecimal depositAmount, int depositPeriod) {
        super(depositAmount, depositPeriod);
    }

    @Override
    public BigDecimal income() {
        int i = 1;
        BigDecimal incomeTotal = super.getAmount();
        while (i <= super.getPeriod())
        {
            incomeTotal = (incomeTotal.multiply(BigDecimal.valueOf(i))).divide(BigDecimal.valueOf(100)).add(incomeTotal).setScale(2,RoundingMode.HALF_EVEN);
            i++;
        }
        return incomeTotal.subtract(super.getAmount()).setScale(2,RoundingMode.HALF_EVEN);

    }

    @Override
    public boolean canToProlong() {
        return (amount.compareTo(new BigDecimal(1000))>0);
    }
    @Override
    public int compareTo(Deposit other) {
        return this.amount.add(income()).compareTo(other.amount.add(other.income()));
    }

    @Override
    public boolean equals(Object obj) {
        if ((obj != null) && (obj.getClass() != this.getClass())) {
            final Deposit deposit = (Deposit) obj;

                return (amount.equals(deposit.amount) && (period == deposit.period));
        }
        return false;
    }

    @Override
    public int hashCode() {

        return amount.hashCode();
    }
}
