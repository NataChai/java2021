package com.epam.test.automation.java.practice8;

import java.math.BigDecimal;
import java.math.RoundingMode;



public class LongDeposit extends Deposit implements Prolongable {


    protected LongDeposit(BigDecimal depositAmount, int depositPeriod) {
        super(depositAmount, depositPeriod);
    }

    @Override
    public BigDecimal income() {
        BigDecimal incomeTotal = super.getAmount();
        if (super.getPeriod() <= 6)
            return BigDecimal.ZERO.setScale(0,RoundingMode.HALF_EVEN);
        else
        {
            int i = 6;
            while (i < super.getPeriod())
            {
                incomeTotal = incomeTotal.multiply(BigDecimal.valueOf(0.15)).add(incomeTotal).setScale(2, RoundingMode.HALF_EVEN);
                i++;
            }
        }
        return incomeTotal.subtract(super.getAmount()).setScale(2, RoundingMode.HALF_EVEN);

    }


    @Override
    public boolean canToProlong() {
        return (period < 36);
    }
    @Override
    public int compareTo(Deposit other) {
        return this.amount.add(income()).compareTo(other.amount.add(other.income()));
    }

    @Override
    public boolean equals(Object obj) {
        if ((obj != null) && (obj.getClass() != this.getClass())) {
            final Deposit deposit = (Deposit) obj;

            return (amount.equals(deposit.amount)) && (period == deposit.period);
        }
        return false;
    }

    @Override
    public int hashCode() {

        return amount.hashCode();
    }
}
